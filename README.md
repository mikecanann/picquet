picquet
=======

Simple process limit, only allowing one process to run at a time.


------

sample usage:
<pre>
   require 'picquet'
   exit if (0 == picquet())
</pre>
Or to overide the default retry and delay values:
<pre>
   require 'picquet'
   exit if (0 == picquet(retry_count, delay_seconds))
</pre>

------

This is a very simple program, if more advanced features are needed use something like [god](https://github.com/mojombo/god) or [bluepill](https://github.com/bluepill-rb/bluepill)
