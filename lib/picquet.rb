def picquet(num_retries = 5, sec_delay = 1)

  (1..num_retries).each do |i|
    if File.open($PROGRAM_NAME).flock(File::LOCK_EX | File::LOCK_NB)
      return(1)
    end
    sleep(sec_delay)
  end

  warn "Program #{$PROGRAM_NAME} is already running"
  return(0)
end

