require 'helper'

class TestPicquet < Test::Unit::TestCase

  context "Locking" do
    should "1 get the lock first time" do
      assert_equal(1, picquet())
    end
    should "2 get the lock a second time will fail" do
      assert_equal(0, picquet(1,1))
    end
  end
end
